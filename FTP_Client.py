from ftplib import FTP
import os

#domain name or server ip /User /Password:
ftp = FTP('test.rebex.net','DEMO','password')

#FTP-Path for download
ftp.cwd('/pub/example/')

src_path = r"C:\Users\jurij\.PyCharmCE2018.3\config\scratches\StartUp\env\src"
dst_path = r"C:\Users\jurij\.PyCharmCE2018.3\config\scratches\StartUp\env\dst"
filename = 'readme.txt'

###Download###
def downloadfile():
    print("######## FTP - Download #########")
    local_filename = os.path.join(src_path, filename)
    lf = open(local_filename, 'wb')
    ftp.retrbinary('RETR ' + filename, lf.write, 1024)
    print("File: " + str(filename) + " - Download completed")

    ftp.quit()
    lf.close()

### Upload###
def uploadfile():
    print("######## FTP - Upload #########")
    local_filename = os.path.join(dst_path, filename)
    file = open(local_filename, 'rb')  # file to send
    ftp.storbinary('STOR ' + 'readme.txt', file)  # send the file
    file.close()  # close file and FTP
    ftp.quit()

#downloadfile()
#uploadfile()